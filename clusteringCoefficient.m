function Ci = clusteringCoefficient(graph, K)
    nNodes = length(graph);
    Ci = zeros(nNodes, 1);
    connections = triu(graph + graph');
    [i, j] = find(connections >= 1);
    for v1 = 1:length(graph)
        neighbors1 = j(i == v1);
        for v2 = neighbors1'
            neighbors2 = j(i == v2);
            mutualNeighbors = intersect(neighbors1, neighbors2);
            if isempty(mutualNeighbors)
                continue
            end

            for v3 = mutualNeighbors'
                summand = (connections(v1, v2) * ...
                           connections(v1, v3) * ...
                           connections(v2, v3));

                Ci([v1, v2, v3]) = Ci([v1, v2, v3]) + summand;
            end
        end
    end
    denominator = ((K .* (K - 1)) - (2 * sum(graph .* graph', 2)));
    % If denominator 0 numerator should be too.
    denominator(denominator == 0) = 1;
    Ci = (0.5 * Ci) ./ denominator;
end
