function kIn = calcInDegrees(graph)
    kIn = sum(graph, 1)';
end
