function hubs = findDegreeHubs(kIn, kOut)
% findDegreeHubs looks for nodes in connectionMatrix with degree greater than
% the 1 standard deviation more than the average node degree of the graph.

    kTotal = kIn + kOut;
    hubs = kTotal > (mean(kTotal) + std(kTotal));
end
