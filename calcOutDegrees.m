function kOut = calcOutDegrees(graph)
    kOut = sum(graph, 2);
end
