function [binaryGraph, threshold] = binarizeConnectionMatrix(...
    connectionMat, ...
    desiredK, ...
    tolerance)

    threshold = 0.1;
    calcK = @(threshold) ...
            mean(smallWorld.calcInDegrees(connectionMat > threshold) + ...
                 smallWorld.calcOutDegrees(connectionMat > threshold));

    K = calcK(threshold);
    deltaK = K - desiredK;
    H = 10 ^ -3;
    MAXITER = 100;
    ETA = 1;
    prevLength = 10;
    prev = zeros(1, prevLength);
    iter = 0;
    while abs(deltaK) > threshold && iter < MAXITER
        dK_dT = (calcK(threshold - H) - calcK(threshold + H)) / (2 * H);
        deltaK = K - desiredK;

        if ismember(deltaK, prev)
            ETA = ETA / 1.2;
        end
        prev(mod(iter, prevLength) + 1) = deltaK;

        threshold = threshold + (ETA * (deltaK / dK_dT));
        K = calcK(threshold);
        iter = iter + 1;
    end

    if iter == MAXITER
        error('MATLAB:UNBOUND', 'Failed to converge on desired K')
    end

    binaryGraph = connectionMat > threshold;
end
