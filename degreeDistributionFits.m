function [degrees, cdf, pdf, expModel, truncPowerModel] = degreeDistributionFits(k)
    k = ceil(k);
    degrees = (max(k):-1:0)';
    pdf = sum(k' == degrees, 2);
    cdf = cumsum(pdf);

    pdf = pdf / cdf(end);
    cdf = cdf / cdf(end);
    expModel = @(k, vars) vars(1) * exp(-vars(2) .* k);
    truncPowerModel = @(k, vars) [0; vars(1) * ((k(k ~= 0)) .^ (vars(2) - 1)) .* exp(-(k(k ~= 0) / vars(3)))];
    expModel = fit(degrees, cdf, expModel, 2, 0.01);
    truncPowerModel = fit(degrees, cdf, truncPowerModel, 3, 0.25);
end

function model = fit(k, cdf, model, nVars, eta)
    vars = ones(1, nVars);
    delta = ones(1, nVars);
    mse = @(vars) mean((model(k, vars) - cdf) .^ 2);

    H = 10 ^ -5;
    EPSILON = 10 ^ -5;
    MAXITER = 10000;

    iter = 0;
    while (mse(vars) > EPSILON) && (iter < MAXITER) && (sum(delta .^ 2) > EPSILON)
        iter = iter + 1;

        for i = 1:nVars
            delta(i) = derivative(i);
        end
        vars = vars - (eta * delta); % * 0.9 ^ iter);
    end
    model = @(k) model(k, vars);

    function delta = derivative(i)
        left = vars; right = vars;
        left(i) = left(i) - H;
        right(i) = right(i) + H;
        delta = (mse(right) - mse(left)) / (2 * H);
    end
end
