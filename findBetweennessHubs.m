function hubs = findBetweennessHubs(Cb)
    hubs = Cb > (mean(Cb) + std(Cb));
end
