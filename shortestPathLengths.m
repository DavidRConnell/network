function Li = shortestPathLengths(d)
    Li = sum(d, 2) ./ (length(d) - 1);
end
