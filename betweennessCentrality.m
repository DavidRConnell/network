function [Cb, d] = betweennessCentrality(binaryGraph)
    nNodes = length(binaryGraph);
    Cb = zeros(nNodes, 1);
    d = zeros(nNodes) - 1;
    P = cell(nNodes, 1);
    Q = queue(nNodes);
    S = queue(nNodes);

    for s = 1:nNodes
        sigma = zeros(nNodes);
        S.reset()
        Q.reset()

        sigma(s) = 1;
        d(s, s) = 0;
        Q.enqueue(s);
        while Q.pointer() > 0
            v = Q.dequeue();
            S.enqueue(v);

            for w = find(binaryGraph(v, :) == 1)
                if d(s, w) < 0
                    Q.enqueue(w);
                    d(s, w) = d(s, v) + 1;
                end

                if d(s, w) == d(s, v) + 1
                    sigma(w) = sigma(w) + sigma(v);

                    if isempty(P{w})
                        P{w} = queue(nNodes);
                    end
                    P{w}.enqueue(v);
                end
            end
        end

        delta = zeros(nNodes, 1);
        while S.pointer() > 0
            w = S.dequeue();
            if ~isempty(P{w})
                while P{w}.pointer() > 0
                    v = P{w}.dequeue();
                    delta(v) = delta(v) + (sigma(v) / sigma(w)) * (1 + delta(w));
                end
            end

            if w ~= s
                Cb(w) = Cb(w) + delta(w);
            end
        end
    end
    Cb = Cb / ((nNodes - 1) * (nNodes - 2));
end

function Q = queue(length)
    list = zeros(length, 1);
    idx = 0;
    Q.enqueue = @enqueue;
    Q.dequeue = @dequeue;
    Q.reset = @reset;
    Q.pointer = @pointer;

    function enqueue(value)
        assert(idx < length, 'Overflow')
        idx = idx + 1;
        list(idx) = value;
    end

    function value = dequeue()
        assert(idx > 0, 'Underflow')
        value = list(idx);
        idx = idx - 1;
    end

    function reset()
        idx = 0;
    end

    function p = pointer()
        p = idx;
    end
end
